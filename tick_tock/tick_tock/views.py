from django.template import loader
from django.contrib.auth.decorators import login_required
from tick_tock.views_utils import default_context
from django.http import HttpResponse

@login_required
def index(request):
    template = loader.get_template('choose.html')
    context = default_context("Home")
    return HttpResponse(template.render(context, request))
