from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^add-entry$', views.add_entry, name='add_entry'),
    url(r'^insert-entry$', views.insert_entry, name='insert_entry'),
]
