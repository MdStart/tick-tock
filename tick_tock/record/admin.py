from django.contrib import admin

from .models import Project, Team, Client

admin.site.register(Client)
admin.site.register(Project)
admin.site.register(Team)
