from django.db import models

from django.contrib.auth.models import User
from tagging.registry import register


class Team(models.Model):
    """
    Model for storing team assignments.
    """
    name = models.CharField(max_length=50, db_index=True)
    members = models.ManyToManyField(User)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)


class Client(models.Model):
    """
    Model for storing client information.
    """
    name = models.CharField(max_length=50, db_index=True)
    address = models.TextField(null=True, blank=True)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)


class Project(models.Model):
    """
    Model for storing projects.
    """
    name = models.CharField(max_length=50, db_index=True)
    description = models.TextField(null=True, blank=True)
    teams = models.ManyToManyField(Team)
    client = models.OneToOneField(Client)

    def __str__(self):
        return str(self.id) + ": " + str(self.name)


class ProjectEntry(models.Model):
    """
    Model for a project entry.
    """
    member = models.CharField(max_length=50, db_index=True)
    project = models.OneToOneField(Project, db_index=True, null=True)
    from_date = models.DateTimeField(db_index=True)
    to_date = models.DateTimeField()
    closed = models.BooleanField(db_index=True, default=False)

    def __str__(self):
        return str(self.id) + " " + str(self.member) + " " + str(self.from_date) + " " + str(self.to_date)


register(ProjectEntry)
