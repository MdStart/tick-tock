from django.template import loader
from django.contrib.auth.models import User
from .models import Project, ProjectEntry
from django.contrib.auth.decorators import login_required, permission_required
from tick_tock.views_utils import get_variable_with_error, default_context
from django.http import HttpResponse, HttpResponseRedirect
from datetime import datetime, timedelta
from tick_tock.error import create_error_response

@login_required
def index(request):
    template = loader.get_template('record/index.html')
    context = default_context("Home")
    return HttpResponse(template.render(context, request))


@login_required
def add_entry(request):
    template = loader.get_template('record/add_entry.html')
    projects = []
    for p in Project.objects.all().order_by("name"):
        projects.append((p.id, p.name))
    context = default_context("Add entry")
    context['projects'] = projects
    tm = datetime.now()
    tm = tm - timedelta(minutes=tm.minute % 5, seconds=tm.second, microseconds=tm.microsecond)
    context['from_date'] = tm.strftime("%Y-%m-%d")
    context['from_time'] = (tm - timedelta(minutes=15)).strftime("%H:%M")
    context['to_date'] = tm.strftime("%Y-%m-%d")
    context['to_time'] = tm.strftime("%H:%M")
    return HttpResponse(template.render(context, request))


@login_required
def insert_entry(request):
    title = "Insert entry"
    project_id = get_variable_with_error(request, title, "project")
    from_date = get_variable_with_error(request, title, "from_date")
    from_time = get_variable_with_error(request, title, "from_time")
    to_date = get_variable_with_error(request, title, "to_date")
    to_time = get_variable_with_error(request, title, "to_time")
    project = Project.objects.get(id=project_id[1])
    if project is None:
        return create_error_response(request, title, "Failed to load project with id: " + str(project_id[1]))
    user = User.objects.get(username=request.user)
    if user is None:
        return create_error_response(request, title, "Failed to load user with name: " + str(request.user))
    entry = ProjectEntry()
    entry.member = user.username
    entry.from_date = from_date[1] + " " + from_time[1]
    entry.to_date = to_date[1] + " " + to_time[1]
    entry.save()
    entry.project = project
    entry.save()
    return HttpResponseRedirect("/record/add-entry")
