from setuptools import setup, find_packages

setup(
    name="tick-tock",
    version="0.0.1",
    license="GNU General Public License version 3.0 (GPLv3)",
    description="Django-based framework for recording work done on project to allow invoicing.",
    author="Peter Reutemann",
    author_email="fracpete@waikato.ac.nz",
    url="https://github.com/fracpete/tick-tock",
    packages=find_packages(),
    install_requires=[
        "Django",
        "django-tagging",
        "django_excel",
        "pyexcel-xls",
        "django-bootstrap3-datetimepicker",
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Programming Language :: Python :: 3",
        "Environment :: Web Environment",
        "Operating System :: OS Independent",
        "Framework :: Django",
    ],
)
